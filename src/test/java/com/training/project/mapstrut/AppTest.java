package com.training.project.mapstrut;

import org.junit.Assert;
import org.junit.Test;

import com.training.mapstrut.mid.model.User;
import com.training.mapstrut.simple.model.Employee;
import com.training.mapstrut.simple.util.UserMapper;

public class AppTest{
    
	@Test
    public void simpleTest(){
        User user = new User();
        user.setId(125);
        user.setName("Chao");
        user.setMarried(false);
        
        Employee e = UserMapper.INSTANCE.userToEmployee(user);
        System.out.println(e);
        Assert.assertFalse(e.isMarried());
    }
	
}
