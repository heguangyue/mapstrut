package com.training.project.mapstrut;

import org.junit.Test;

import com.training.mapstrut.mid.model.Employee;
import com.training.mapstrut.mid.model.User;
import com.training.mapstrut.mid.util.UserMapper;

public class MidTest {
	@Test
	public void midTest(){
		User user = new User();
        user.setId(125);
        user.setName("Lee");
        user.setMarried(true);
        
        Employee e = UserMapper.INSTANCE.userToEmployee(user);
        System.out.println(e);
	}
	
	@Test
	public void midTest2(){
		Employee e = new Employee();
        e.setId(222);
        e.setEname("Chao");
        e.setMarried("N");
        
        User u = UserMapper.INSTANCE.employeeToUser(e);
        System.out.println(u);
	}
}
