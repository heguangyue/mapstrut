package com.training.project.mapstrut;

import org.junit.Test;

import com.training.mapstrut.enums.model.User;
import com.training.mapstrut.enums.model.User.Grade;
import com.training.mapstrut.enums.model.UserDao;
import com.training.mapstrut.enums.model.UserDojo;
import com.training.mapstrut.enums.util.UserMapper;


public class EnumTest {
	
/*	@Test
    public void test1(){
        User user = new User();
        user.setGrade(Grade.DIAMOND);
        UserDao e = UserMapper.INSTANCE.userToUserDao(user);
        System.out.println(e);
    }*/
	
	@Test
    public void test1(){
        User user = new User();
        user.setGrade(Grade.DIAMOND);
        UserDojo u = UserMapper.INSTANCE.userToUserDojo(user);
        System.out.println(u);
    }
}
