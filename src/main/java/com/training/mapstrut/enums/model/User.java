package com.training.mapstrut.enums.model;

public class User {
	
	public enum Grade{
		AVERAGE,BRONZE,GOLD,DIAMOND;
	}
	
	private Grade grade;

	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	@Override
	public String toString() {
		return "User [grade=" + grade + "]";
	}
	
}
