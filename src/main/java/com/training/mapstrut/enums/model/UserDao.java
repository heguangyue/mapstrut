package com.training.mapstrut.enums.model;

public class UserDao {
	
	public enum Level{
		AVERAGE,BRONZE,GOLD,DIAMOND;
	}
	
	private Level level;

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	@Override
	public String toString() {
		return "UserDao [level=" + level + "]";
	}
	
}
