package com.training.mapstrut.enums.model;

public class UserDojo {
	
	public enum RANK{
		NORMAL,JUNIOR,SENIOR,MASTER;
	}
	
	private RANK rank;

	public RANK getRank() {
		return rank;
	}

	public void setRank(RANK rank) {
		this.rank = rank;
	}

	@Override
	public String toString() {
		return "UserDojo [rank=" + rank + "]";
	}

}
