package com.training.mapstrut.enums.model;

public class Employee {
	
	public enum Position{
		OFFICER,MANAGER,AVP,SVP;
	}
	
	private Position position;

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return "Employee [position=" + position + "]";
	}
	
}
