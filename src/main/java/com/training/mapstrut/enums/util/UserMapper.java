package com.training.mapstrut.enums.util;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.Mappings;
import org.mapstruct.ValueMapping;
import org.mapstruct.ValueMappings;
import org.mapstruct.factory.Mappers;

import com.training.mapstrut.enums.model.User;
import com.training.mapstrut.enums.model.User.Grade;
import com.training.mapstrut.enums.model.UserDao;
import com.training.mapstrut.enums.model.UserDojo;
import com.training.mapstrut.enums.model.UserDojo.RANK;

@Mapper
public interface UserMapper {

	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
	
	@Mappings({
		@Mapping(source="grade", target="level")
	})
	UserDao userToUserDao(User user);
	
	@Mappings({
		@Mapping(source="grade", target="rank")
	})
	UserDojo userToUserDojo(User user);
	
	@ValueMappings({
		@ValueMapping(source="AVERAGE",target="NORMAL"),
		@ValueMapping(source="BRONZE",target="JUNIOR"),
		@ValueMapping(source="GOLD",target="SENIOR"),
		@ValueMapping(source="DIAMOND",target="MASTER"),
		@ValueMapping(source=MappingConstants.ANY_UNMAPPED, target=MappingConstants.NULL)
	})
	RANK customConverter(Grade grade);
	
/*	@Mappings({
		@Mapping(source="grade", target="position")
	})
	Employee userToEmployee(User user);
*/
//	User employeeToUser(Employee employee);
}
