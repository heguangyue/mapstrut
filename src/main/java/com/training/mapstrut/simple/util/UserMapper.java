package com.training.mapstrut.simple.util;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.training.mapstrut.mid.model.User;
import com.training.mapstrut.simple.model.Employee;

@Mapper
public interface UserMapper {

	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
	
	Employee userToEmployee(User user);
	
	User employeeToUser(Employee employee);
}
