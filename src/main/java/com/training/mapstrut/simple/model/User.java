package com.training.mapstrut.simple.model;

public class User {
	private int id;
	private String name;
	private boolean married;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isMarried() {
		return married;
	}

	public void setMarried(boolean married) {
		this.married = married;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", married=" + married + "]";
	}

}
