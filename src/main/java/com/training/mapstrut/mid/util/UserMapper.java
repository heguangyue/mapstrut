package com.training.mapstrut.mid.util;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.training.mapstrut.mid.model.Employee;
import com.training.mapstrut.mid.model.User;

@Mapper(uses = UserTransform.class)
public interface UserMapper {

	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
	
	@Mappings({
		@Mapping(source="name", target="ename")
	})
	Employee userToEmployee(User user);

	@Mappings({
		@Mapping(source="ename", target="name")
	})
	User employeeToUser(Employee employee);
}
