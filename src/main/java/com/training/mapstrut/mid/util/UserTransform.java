package com.training.mapstrut.mid.util;

public class UserTransform {
	
	public String booleanToString(boolean value){
		if(value){
			return "Y";
		}
		return "N";
	}
	
	public boolean strToBoolean(String str){
		if ("Y".equals(str)) {
			return true;
		}
		return false;
	}
	
}
