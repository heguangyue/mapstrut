package com.training.mapstrut.mid.model;

public class Employee {
	private int id;
	private String ename;
	private String position;
	private String married;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getMarried() {
		return married;
	}
	public void setMarried(String married) {
		this.married = married;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", ename=" + ename + ", position=" + position + ", married=" + married + "]";
	}

}
